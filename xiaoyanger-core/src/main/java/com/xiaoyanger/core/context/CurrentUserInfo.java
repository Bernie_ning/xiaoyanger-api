package com.xiaoyanger.core.context;

import com.xiaoyanger.core.constant.Constant;
import com.xiaoyanger.core.exception.BusinessException;
import com.xiaoyanger.core.utils.SpringContextUtils;
import org.springframework.util.StringUtils;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

/**
 * @author ningjiangbo
 * @since 2023/04/24 15:20
 */
public class CurrentUserInfo {

    public static CommonUserInfoDTO getCurrUserInfo() {

        String userId = SpringContextUtils.getHttpServletRequest().getHeader(Constant.X_USERID);
        String username = SpringContextUtils.getHttpServletRequest().getHeader(Constant.X_USERNAME);
        String realname = SpringContextUtils.getHttpServletRequest().getHeader(Constant.X_REALNAME);
        String platform = SpringContextUtils.getHttpServletRequest().getHeader(Constant.X_PLATFORM);
        if (StringUtils.isEmpty(userId)) {
            throw new BusinessException("无 token, 无法获取当前登录用户信息", -1);
        }
        CommonUserInfoDTO commonUserInfoDTO = new CommonUserInfoDTO();
        commonUserInfoDTO.setId(URLDecoder.decode(userId, StandardCharsets.UTF_8));
        commonUserInfoDTO.setUsername(username != null ? URLDecoder.decode(username, StandardCharsets.UTF_8) : null);
        commonUserInfoDTO.setRealname(realname != null ? URLDecoder.decode(realname, StandardCharsets.UTF_8) : null);
        commonUserInfoDTO.setPlatform(platform != null ? URLDecoder.decode(platform, StandardCharsets.UTF_8) : null);
        return commonUserInfoDTO;
    }

}