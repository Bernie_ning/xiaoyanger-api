package com.xiaoyanger.core.context;

import lombok.Data;

import java.io.Serializable;

/**
 * @author ningjiangbo
 * @since 2023/04/24 15:21
 */
@Data
public class CommonUserInfoDTO implements Serializable {

    private static final long serialVersionUID = 328457623042964636L;

    /**
     * 标识
     */
    private String id;

    /**
     * 登录账号 username phone
     */
    private String username;

    /**
     * 姓名
     */
    private String realname;

    /**
     * 平台
     */
    private String platform;

}
