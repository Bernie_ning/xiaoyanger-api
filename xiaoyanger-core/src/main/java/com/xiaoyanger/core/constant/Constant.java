package com.xiaoyanger.core.constant;

/**
 * @author ningjiangbo
 * @since 2023/4/14 9:35
 */
public final class Constant {

    private Constant(){}
    /**
     * 当前页码
     */
    public static final String CURR_PAGE = "page";
    /**
     * 每页显示记录数
     */
    public static final String PAGE_SIZE = "size";
    /**
     * 排序字段
     */
    public static final String ORDER_FIELD = "sidx";
    /**
     * 排序方式
     */
    public static final String ORDER = "order";
    /**
     * 升序
     */
    public static final String ASC = "asc";
    /**
     * 降序
     */
    public static final String DESC = "desc";

    /**
     * token
     */
    public static final String TOKEN = "token";
    /**
     * redis 工具类实例beanName
     */
    public static final String REDIS_UTILS_INSTANCE = "redisUtils";

    /**
     * 创建者id
     */
    public static final String CREATE_ID = "createBy";

    /**
     * 创建时间
     */
    public static final String CREATE_TIME = "createTime";

    /**
     * 更新者id
     */
    public static final String UPDATE_ID = "updateBy";

    /**
     * 更新者时间
     */
    public static final String UPDATE_TIME = "updateTime";

    /**
     * 逻辑删除字段
     */
    public static final String DELETED = "deleted";

    /**
     * 版本字段
     */
    public static final String REVISION = "revision";

    /**
     * X-Platform
     */
    public static final String X_PLATFORM = "X-Platform";
    /**
     * X-Access-Token
     */
    public static final String X_ACCESS_TOKEN = "X-Access-Token";
    /**
     * X-ClientIp
     */
    public static final String X_CLIENTIP = "X-ClientIp";
    /**
     * X-UserId
     */
    public static final String X_USERID = "X-UserId";
    /**
     * X-TraceId
     */
    public static final String X_TRACEID = "X-TraceId";
    /**
     * X-UserName
     */
    public static final String X_USERNAME = "X-UserName";
    /**
     * X-Realname
     */
    public static final String X_REALNAME = "X-Realname";

}
