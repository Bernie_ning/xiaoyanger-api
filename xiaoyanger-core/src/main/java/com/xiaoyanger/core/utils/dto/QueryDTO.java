package com.xiaoyanger.core.utils.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author ningjiangbo
 * @since 2023/4/14 10:15
 */
@Data
public class QueryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 当前页
     */
    private Long page = 1L;

    /**
     * 每页显示数量
     */
    private Long size = 10L;

    /**
     * 排序方式
     * sort = "{\"field1Name\":\"ASC\", \"field2Name\":\"DESC\"}"
     */
    private LinkedHashMap<String, String> sort = new LinkedHashMap<>();

    /**
     * 查询条件
     * query = "{\"id\":\"1\", \"name\":\"cc\"}")
     */
    private Map<String, Object> query = new HashMap<>();


}
