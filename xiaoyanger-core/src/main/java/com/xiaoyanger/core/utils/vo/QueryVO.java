package com.xiaoyanger.core.utils.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author ningjiangbo
 * @since 2023/4/13 18:17
 */
@Data
@ApiModel(value="Query对象", description="分页查询对象")
public class QueryVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "当前页", example = "1")
    private Long page = 1L;

    @ApiModelProperty(value = "每页显示数量", example = "10")
    private Long size = 10L;

    @ApiModelProperty(value = "排序方式", example = "{\"field1Name\":\"ASC\", \"field2Name\":\"DESC\"}")
    private LinkedHashMap<String, String> sort = new LinkedHashMap<>();

    @ApiModelProperty(value = "查询条件", example = "{\"id\":\"1\", \"name\":\"cc\"}")
    private Map<String, Object> query = new HashMap<>();


}