package com.xiaoyanger.core.utils.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author ningjiangbo
 * @since 2023/4/13 18:17
 */
@Data
@ApiModel(value = "ResultDataVO对象", description = "返回单个结果对象")
public class ResultDataVO<T> extends ResultVO<T> {

    @ApiModelProperty("返回数据")
    private T data;

    public static <T> ResultDataVO<T> ok(T t) {
        ResultDataVO<T> r = new ResultDataVO<>();
        r.setData(t);
        return r;
    }

    public static <T> ResultDataVO<T> ok(T t, String msg) {
        ResultDataVO<T> r = new ResultDataVO<>();
        r.setMessage(msg);
        r.setData(t);
        return r;
    }

}