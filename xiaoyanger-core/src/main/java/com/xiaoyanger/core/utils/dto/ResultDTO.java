package com.xiaoyanger.core.utils.dto;

import com.xiaoyanger.core.exception.ErrorType;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ningjiangbo
 * @since 2023/4/14 10:15
 */
@Data
public class ResultDTO<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final int OK_CODE = 0;

    private Integer code;

    private String msg;

    private T data;

    public ResultDTO() {
        this.code = OK_CODE;
        this.msg = "success";
    }

    public static <T> ResultDTO<T> error(ErrorType errorType) {
        return error(errorType.getCode(), errorType.getMsg());
    }

    public static <T> ResultDTO<T> error() {
        return error(500, "系统出现异常，请联系管理员");
    }

    public static <T> ResultDTO<T> error(String msg) {
        return error(500, msg);
    }

    public static <T> ResultDTO<T> error(int code, String msg) {
        ResultDTO<T> r = new ResultDTO<>();
        r.setCode(code);
        r.setMsg(msg);
        return r;
    }

    public static <T> ResultDTO<T> ok(T t) {
        ResultDTO<T> r = new ResultDTO<>();
        r.setData(t);
        return r;
    }
    public static <T> ResultDTO<T> ok(T t, String msg) {
        ResultDTO<T> r = new ResultDTO<>();
        r.setMsg(msg);
        r.setData(t);
        return r;
    }

    public static <T> ResultDTO<T> ok(int code, T t) {
        ResultDTO<T> r = new ResultDTO<>();
        r.setCode(code);
        r.setData(t);
        return r;
    }

    public static <T> ResultDTO<T> ok() {
        return new ResultDTO<>();
    }

}

