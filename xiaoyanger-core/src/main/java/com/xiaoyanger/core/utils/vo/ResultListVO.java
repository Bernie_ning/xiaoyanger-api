package com.xiaoyanger.core.utils.vo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author ningjiangbo
 * @since 2023/4/14 10:29
 */
@Data
@ApiModel(value = "ResultListVO对象", description = "返回列表结果对象")
public class ResultListVO<T> extends ResultVO<T> {

    @ApiModelProperty(value = "列表集合")
    private List<T> list;

    @ApiModelProperty("页面索引 1开始")
    private Long page;

    @ApiModelProperty("每页大小")
    private Long size;

    @ApiModelProperty("总数据数")
    private Long total;

    @ApiModelProperty("总页数")
    private Long page_total;

    @ApiModelProperty("是否有下一页")
    private boolean next_page;


    public static <T> ResultListVO<T> ok(List<T> t) {
        ResultListVO<T> r = new ResultListVO<>();
        r.setList(t);
        return r;
    }

    public static <T> ResultListVO<T> ok(List<T> t, String msg) {
        ResultListVO<T> r = new ResultListVO<>();
        r.setMessage(msg);
        r.setList(t);
        return r;
    }

    public static <T> ResultListVO<T> ok(IPage<T> page) {
        ResultListVO<T> r = new ResultListVO<>();
        r.fromPage(page);
        return r;
    }

    public void setList(List<T> tList) {
        if (tList != null) {
            list = tList;
            this.page = 1L;
            this.size = Long.valueOf(tList.size());
            this.total = Long.valueOf(tList.size());
            this.page_total = 1L;
            this.next_page = false;
        }

    }
    /**
     * 分页
     */
    private void fromPage(IPage<T> page) {
        if (page != null) {
            if(page instanceof Page){
                this.next_page = ((Page<T>) page).hasNext();
            }
            this.total = page.getTotal();
            this.page_total = page.getPages();
            this.page = page.getCurrent();
            this.size = page.getSize();
            this.list = page.getRecords();
        }
    }


}
