package com.xiaoyanger.core.utils.vo;

import com.xiaoyanger.core.exception.ErrorType;
import com.xiaoyanger.core.exception.SystemErrorType;
import com.xiaoyanger.core.utils.HttpRequestUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ningjiangbo
 * @since 2023/4/13 18:17
 */
@Data
@ApiModel(value = "ResultVO对象", description = "返回结果对象")
public class ResultVO<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final int OK_CODE = 0;

    @ApiModelProperty(value = "返回状态码", dataType = "int")
    private Integer code;

    @ApiModelProperty("消息描述")
    private String message;

    public ResultVO() {
        this.code = OK_CODE;
        this.message = "success";
    }

    public Boolean getSuccess() {
        var success = this.code == OK_CODE;
        if (!success) {
            HttpRequestUtil.setRecordResponse(true);
        }
        return success;
    }

    public long getDuration() {
        return HttpRequestUtil.getRequestDuration();
    }

    @ApiModelProperty(value = "服务器时间")
    public Long getTimestamp() {
        return System.currentTimeMillis() / 1000;
    }

    public static <T> ResultVO<T> error(ErrorType errorType) {
        return error(errorType.getCode(), errorType.getMsg());
    }

    public static <T> ResultVO<T> error() {
        return error(500, "未知异常，请联系管理员");
    }

    public static <T> ResultVO<T> paramError(String msg) {
        return error(SystemErrorType.ARGUMENT_NOT_VALID.getCode(), msg);
    }

    public static <T> ResultVO<T> error(String msg) {
        return error(500, msg);
    }

    public static <T> ResultVO<T> error(int code, String msg) {
        ResultVO<T> r = new ResultVO<>();
        r.setCode(code);
        r.setMessage(msg);
        return r;
    }

    public static <T> ResultVO<T> ok() {
        return new ResultVO<>();
    }

}