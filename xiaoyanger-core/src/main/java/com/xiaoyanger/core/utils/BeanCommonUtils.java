package com.xiaoyanger.core.utils;

import com.xiaoyanger.core.exception.BusinessException;
import org.springframework.beans.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

/**
 * @author ningjiangbo
 * @since 2023/4/14 10:16
 */
public class BeanCommonUtils {

    private BeanCommonUtils(){}

    /**
     * @param source 源
     * @param target 目标
     */
    public static void copyProperties(Object source, Object target) {
        Optional<Object> sourceTemp = Optional.ofNullable(source);
        Optional<Object> targetTemp = Optional.ofNullable(target);
        sourceTemp.flatMap(c -> targetTemp).ifPresent(t -> {
            BeanUtils.copyProperties(source, target);
        });
    }

    /**
     * @param source     源
     * @param target     目标
     * @param targetBean 目标对象
     */
    public static <F, T> void copyListProperties(List<F> source, List<T> target, Class<T> targetBean) {
        Optional<Object> sourceListTemp = Optional.ofNullable(source);
        Optional<Object> targetListTemp = Optional.ofNullable(target);
        sourceListTemp.flatMap(x -> targetListTemp).ifPresent(y -> {
            source.forEach(c -> {
                T newInstance;
                try {
                    newInstance = targetBean.getDeclaredConstructor().newInstance();
                    Optional<Object> sourceTemp = Optional.ofNullable(c);
                    sourceTemp.ifPresent(t->{
                        BeanUtils.copyProperties(c, newInstance);
                        target.add(newInstance);
                    });
                } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
                    throw new BusinessException("对象转换异常");
                }
            });
        });

    }

}
