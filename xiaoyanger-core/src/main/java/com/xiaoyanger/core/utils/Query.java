package com.xiaoyanger.core.utils;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaoyanger.core.constant.Constant;
import com.xiaoyanger.core.utils.dto.QueryDTO;
import com.xiaoyanger.core.utils.vo.QueryVO;
import com.xiaoyanger.core.xss.SQLFilter;

import java.util.*;

/**
 * @author ningjiangbo
 * @since 2023/4/13 18:24
 */
public class Query<T> {

    @Deprecated
    public IPage<T> getPage(Map<String, Object> params) {
        return this.getPage(params, null, false);
    }

    @Deprecated
    public IPage<T> getPage(Map<String, Object> params, String defaultOrderField, boolean isAsc) {
        //分页参数
        long currPage = 1;
        long pageSize = 10;

        if(params.get(Constant.CURR_PAGE) != null && !"".equals((String)params.get(Constant.CURR_PAGE))){
            currPage = Long.parseLong((String)params.get(Constant.CURR_PAGE));
        }
        if(params.get(Constant.PAGE_SIZE) != null && !"".equals((String)params.get(Constant.PAGE_SIZE))){
            pageSize = Long.parseLong((String)params.get(Constant.PAGE_SIZE));
        }

        //分页对象
        Page<T> page = new Page<>(currPage, pageSize);

        //分页参数
        params.put(Constant.CURR_PAGE, page);

        //排序字段
        //防止SQL注入（因为sidx、order是通过拼接SQL实现排序的，会有SQL注入风险）
        String orderField = SQLFilter.sqlInject((String)params.get(Constant.ORDER_FIELD));
        String order = (String)params.get(Constant.ORDER);

        //前端字段排序
        if(StringUtils.isNotEmpty(orderField) && StringUtils.isNotEmpty(order)){
            if(Constant.ASC.equalsIgnoreCase(order)) {
                page.setOrders(Arrays.asList(new OrderItem(orderField,true)));
                return page;
            }else {
                page.setOrders(Arrays.asList(new OrderItem(orderField,false)));
                return page;
            }
        }

        //默认排序
        page.setOrders(Arrays.asList(new OrderItem(defaultOrderField,isAsc)));

        return page;
    }

    public IPage<T> getPage(QueryVO queryVO) {
        //分页参数
        long currPage = 1;
        long pageSize = 10;

        if(queryVO.getPage() != null){
            currPage = queryVO.getPage();
        }
        if(queryVO.getSize() != null){
            pageSize = queryVO.getSize();
        }

        //分页对象
        Page<T> page = new Page<>(currPage, pageSize);

        //排序字段
        LinkedHashMap<String, String> sortList = queryVO.getSort();
        List<OrderItem> orderItems = new ArrayList<>();
        if(CollectionUtil.isNotEmpty(sortList)){
            for(Map.Entry<String,String> sort : sortList.entrySet()){
                //防止SQL注入（因为是通过拼接SQL实现排序的，会有SQL注入风险）
                String orderField = SQLFilter.sqlInject(sort.getKey());
                if(Constant.ASC.equalsIgnoreCase(sort.getValue())) {
                    orderItems.add(new OrderItem(orderField,true));
                }
                if(Constant.DESC.equalsIgnoreCase(sort.getValue())){
                    orderItems.add(new OrderItem(orderField,false));
                }
            }
        }

        //默认排序
        page.setOrders(orderItems);

        return page;
    }

    public IPage<T> getPage(QueryDTO queryDTO) {
        //分页参数
        long currPage = 1;
        long pageSize = 10;

        if(queryDTO.getPage() != null){
            currPage = queryDTO.getPage();
        }
        if(queryDTO.getSize() != null){
            pageSize = queryDTO.getSize();
        }

        //分页对象
        Page<T> page = new Page<>(currPage, pageSize);

        //排序字段
        LinkedHashMap<String, String> sortList = queryDTO.getSort();
        List<OrderItem> orderItems = new ArrayList<>();
        if(CollectionUtil.isNotEmpty(sortList)){
            for(Map.Entry<String,String> sort : sortList.entrySet()){
                //防止SQL注入（因为是通过拼接SQL实现排序的，会有SQL注入风险）
                String orderField = SQLFilter.sqlInject(sort.getKey());
                if(Constant.ASC.equalsIgnoreCase(sort.getValue())) {
                    orderItems.add(new OrderItem(orderField,true));
                }
                if(Constant.DESC.equalsIgnoreCase(sort.getValue())){
                    orderItems.add(new OrderItem(orderField,false));
                }
            }
        }

        //默认排序
        page.setOrders(orderItems);

        return page;
    }
}