package com.xiaoyanger.core.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * @author ningjiangbo
 * @since 2023/4/13 18:19
 */
@Component
@Configuration
@Slf4j
public class HttpRequestUtil {

    public static String getRequestUrl() {
        var request = getRequest();
        if (request != null) {
            return request.getRequestURI().toLowerCase(Locale.ROOT);
        }
        return null;
    }

    public static <T> boolean setAttribute(String key, T value) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (requestAttributes != null) {
            requestAttributes.setAttribute(key, value, 0);
            return true;
        }
        return false;
    }

    public static Object getAttribute(String key) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (requestAttributes != null) {
            return requestAttributes.getAttribute(key, 0);
        }
        return null;
    }

    public static <T> T getAttribute(String key, Class<T> tClass) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (requestAttributes != null) {
            return (T) requestAttributes.getAttribute(key, 0);
        }
        return null;
    }

    public static HttpServletRequest getRequest() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (requestAttributes != null) {
            HttpServletRequest request = requestAttributes.getRequest();
            return request;
        }
        return null;
    }

    public static HttpServletResponse getResponse() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (requestAttributes != null) {
            HttpServletResponse request = requestAttributes.getResponse();
            return request;
        }
        return null;
    }


    public static String getHeader(String name) {
        var request = getRequest();
        if (request != null) {
            return request.getHeader(name);
        }
        return null;
    }

    public static void initRequestStartTime(HttpServletRequest request) {
        request.setAttribute("start_time", System.currentTimeMillis());
    }

    public static long getRequestDuration() {
        var startTime = HttpRequestUtil.getAttribute("start_time", Long.class);
        if (startTime != null) {
            var duration = System.currentTimeMillis() - startTime;
            return duration;
        }
        return -1;
    }

    /**
     * 获取是否需要记录 响应结果
     *
     * @return
     */
    public static boolean recordResponse() {
        return Boolean.TRUE.equals(HttpRequestUtil.getAttribute("record_response", boolean.class));
    }

    /**
     * 设置需要记录 响应结果
     */
    public static void setRecordResponse(boolean recordResponse) {
        HttpRequestUtil.setAttribute("record_response", recordResponse);
    }

}

