package com.xiaoyanger.core.exception;

/**
 * @author ningjiangbo
 * @since 2023/4/13 18:22
 */
public interface ErrorType {
    /**
     * 返回code
     *
     * @return
     */
    int getCode();

    /**
     * 返回msg
     *
     * @return
     */
    String getMsg();
}
