package com.xiaoyanger.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author ningjiangbo
 * @since 2023/04/24 15:28
 */
@SpringBootApplication
@EnableDiscoveryClient //开启服务注册
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }
}
