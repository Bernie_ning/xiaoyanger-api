package com.xiaoyanger.gateway.constant;

import lombok.Getter;

/**
 * @author ningjiangbo
 * @since 2023/4/20 17:52
 */
public enum Platform {

    /**
     *
     */
    None(0, "无"),
    H5(1, "H5"),
    WxApp(2, "微信小程序"),
    PC(4, "PC"),
    APP(8, "APP"),
    MP(16, "MP商户平台"),
    Task(32, "系统任务");

    @Getter
    private int value;

    @Getter
    private final String desc;

    Platform(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    /**
     * 增加
     *
     * @param platforms
     * @return
     */
    public int add(Platform[] platforms) {
        int result = 0;
        for (Platform platform : platforms) {
            result = result + platform.value;
        }
        this.value = result;
        return result;
    }

    /**
     * 减少
     *
     * @param platforms
     * @return
     */
    public int subtract(Platform[] platforms) {
        int result = this.value;
        for (Platform platform : platforms) {
            result = result - platform.value;
        }
        this.value = result;
        return result;
    }

    /**
     * 判断是否有权限
     *
     * @param value
     * @return
     */
    public boolean have(int value) {
        return (this.value & value) == this.value;
    }

    public boolean have(int value, int value2) {
        return (value & value2) == value2;
    }

}
