package com.xiaoyanger.gateway.filter;

import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xiaoyanger.gateway.config.GatewayRoutersConfig;
import com.xiaoyanger.gateway.constant.Platform;
import com.xiaoyanger.gateway.model.UserSessionModel;
import com.xiaoyanger.gateway.utils.SecurityRedisUtil;
import com.xiaoyanger.gateway.utils.TokenUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ningjiangbo
 * @since 2023/04/24 15:56
 */
@SuppressWarnings("ALL")
@Slf4j
@Component
public class TokenGlobalFilter implements GlobalFilter, Ordered {

    @Autowired
    GatewayRoutersConfig gatewayRoutersConfiguration;

    @Autowired
    private SecurityRedisUtil securityRedisUtil;

    @Autowired
    private TokenUtil tokenUtil;

    ObjectMapper mapper = new ObjectMapper();

    @SneakyThrows
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        StopWatch sw = new StopWatch("filter");
        sw.start();

        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        String currentPath = request.getURI().getPath();

        UserSessionModel userSession = null;
        String responseContent = null;

        try {

            String platformStr = tokenUtil.getParam(request, "X-Platform");
            String token = tokenUtil.getParam(request, TokenUtil.X_ACCESS_TOKEN);

            if (StrUtil.isNotBlank(token)) {
                userSession = tokenUtil.verifyToken(token, platformStr);
            }

            // 部分接口直接返回结果 不需要走 代理
            if (currentPath.toLowerCase().equalsIgnoreCase("/session")) {
                JsonNode result = null;

                if (userSession != null) {
                    result = tokenUtil.buildBody(userSession, 0, "ok");
                } else {
                    result = tokenUtil.buildBody(null, -1, "请登录后重试");
                }

                byte[] bits = mapper.writeValueAsBytes(result);
                DataBuffer buffer = response.bufferFactory().wrap(bits);
                response.getHeaders().add("Content-Type", "application/json;charset=UTF-8");

                response.setStatusCode(HttpStatus.OK);

                return response.writeWith(Mono.just(buffer));
            }

            UserSessionModel finalUserSession = userSession;
            ServerHttpRequest proxyToRequest = exchange.getRequest().mutate().headers(headers -> {

                headers.remove("X-UserId");
                headers.remove(TokenUtil.X_ACCESS_TOKEN);
                headers.remove("X-Realname");

                Platform platform = Enum.valueOf(Platform.class, platformStr);
                headers.set("X-Platform", platform.name());

                // 表示从网关经过
                headers.set("X-Gateway", token);

                // 请求前台接口
                String requestURI = exchange.getRequest().getPath().pathWithinApplication().value();
                log.info(requestURI);
                headers.set("X-ClientIp", tokenUtil.getClientIp(request));
                if (finalUserSession != null) {
                    if (finalUserSession.getUserId() != null) {
                        headers.set("X-UserId", URLEncoder.encode(finalUserSession.getUserId(), StandardCharsets.UTF_8));
                    }
                    if (finalUserSession.getUserName() != null) {
                        headers.set("X-UserName", URLEncoder.encode(finalUserSession.getUserName(), StandardCharsets.UTF_8));
                    }
                    if (finalUserSession.getName() != null) {
                        headers.set("X-Realname", URLEncoder.encode(finalUserSession.getName(), StandardCharsets.UTF_8));
                    }
                    headers.set(TokenUtil.X_ACCESS_TOKEN, token);
                }

            }).build();

            if (userSession == null) {
                for (String passUrl : gatewayRoutersConfiguration.getPathExcludes()) {
                    if (currentPath.toLowerCase().contains((passUrl.trim()).toLowerCase())) {
                        return chain.filter(exchange);
                    }
                }

                JsonNode result = tokenUtil.buildBody(null, -1, "请登录后重试");
                byte[] bits = mapper.writeValueAsBytes(result);
                DataBuffer buffer = response.bufferFactory().wrap(bits);
                response.getHeaders().add("Content-Type", "application/json;charset=UTF-8");

                response.setStatusCode(HttpStatus.OK);

                return response.writeWith(Mono.just(buffer));
            }

            return chain.filter(exchange.mutate().request(proxyToRequest).build());
        } catch (Exception ex) {
            log.error("网关调用内部服务异常", ex);
        } finally {
            sw.stop();
            long duration = sw.getTotalTimeMillis();

            Map<String, String> logMap = new HashMap<>();

            try {

                logMap.put("ip", request.getRemoteAddress().toString());
                logMap.put("method", request.getMethod().name());
                logMap.put("url", request.getURI().getPath());
                logMap.put("queryString", request.getURI().getQuery());

                if (userSession != null) {
                    logMap.put("platform", userSession.getPlatform());
                    logMap.put("userId", userSession.getUserId());
                    logMap.put("userName", userSession.getUserName());
                    logMap.put("token", userSession.getToken());
                }

                logMap.put("duration", String.valueOf(duration));

            } catch (Exception exception) {
                log.error("记录日志报错", exception);
            } finally {
                MDC.setContextMap(logMap);
            }

            if (!"/".equals(currentPath)) {
                log.info("请求日志.................................");
            }

            MDC.clear();
        }

        return null;
    }

    @Override
    public int getOrder() {
        return 0;
    }

}
