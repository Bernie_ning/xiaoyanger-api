package com.xiaoyanger.gateway.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * @author ningjiangbo
 * @since 2023/04/24 16:42
 */
@Data
public class UserSessionModel implements Serializable {

    private String token;
    private String userId;
    private String platform;
    private String userName;
    private String userType;
    private Date loginTime;
    private Date refreshTime;
    private String nickName;
    private String name;
    private Map<String, String> extData;

}
