package com.xiaoyanger.gateway.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @author ningjiangbo
 * @since 2023/04/24 16:46
 */
@Slf4j
@Component
@Data
@RefreshScope
public class GatewayRoutersConfig {

    @Value("${spring.cloud.gateway.excludes:}")
    private String[] pathExcludes;

}
