package com.xiaoyanger.gateway.utils;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xiaoyanger.gateway.model.UserSessionModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

/**
 * @author ningjiangbo
 * @since 2023/04/24 16:47
 */
@Component
@Slf4j
public class TokenUtil {

    public static final String X_ACCESS_TOKEN = "X-Access-Token";

    @Autowired
    private SecurityRedisUtil securityRedisUtil;

    ObjectMapper mapper = new ObjectMapper();

    public String getParam(ServerHttpRequest request, String tokenName) {
        String value = parseWsQueryParam(request, tokenName);
        String headerValue = parseHeader(request, tokenName);
        var token = isWebSocket(request)
                ? !StringUtils.isEmpty(value) ? value : headerValue
                : headerValue;
        return token;
    }

    public boolean isWebSocket(ServerHttpRequest serverHttpRequest) {
        return serverHttpRequest.getHeaders().containsKey("Sec-WebSocket-Key");
    }

    public String parseWsQueryParam(ServerHttpRequest serverHttpRequest, String name) {
        var params = serverHttpRequest.getQueryParams();
        return params.getFirst(name.toLowerCase(Locale.ROOT));
    }

    public String parseHeader(ServerHttpRequest serverHttpRequest, String name) {
        var currentHeaders = serverHttpRequest.getHeaders();
        var list = currentHeaders.get(name);
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }

    public JsonNode buildBody(Object data, int code, String errorMessage) {
        var json = mapper.createObjectNode();
        json.put("code", code);
        json.put("message", errorMessage);
        json.put("timestamp", System.currentTimeMillis() / 1000);

        if (data != null) {
            json.putPOJO("data", data);
        }

        return json;
    }

    public UserSessionModel verifyToken(String token, String platform) {
        Assert.notBlank(token, "token不能为空", new Object[0]);
        Assert.notBlank(platform, "platform不能为空", new Object[0]);
        String tokenKey = "Access-Token:" + platform + ":" + token;
        Object redisResult = securityRedisUtil.get(tokenKey);
        if (redisResult != null) {
            Map<String, Object> map = (Map<String, Object>) redisResult;
            ObjectMapper objectMapper = new ObjectMapper();
            UserSessionModel userSession = objectMapper.convertValue(map, UserSessionModel.class);
            this.refreshToken(userSession);
            return userSession;
        } else {
            return null;
        }
    }

    public boolean refreshToken(UserSessionModel userSession) {
        try {
            String token = userSession.getToken();
            Date loginTime = userSession.getLoginTime();
            if (loginTime == null) {
                userSession.setLoginTime(DateTime.now());
            }

            userSession.setRefreshTime(DateTime.now());
            String platform = userSession.getPlatform();
            Assert.notBlank(token, "token不能为空", new Object[0]);
            Assert.notBlank(platform, "platform不能为空", new Object[0]);
            String tokenKey = "Access-Token:" + platform + ":" + token;
            String userSessionKey = "UserSessionModel:" + userSession.getUserId();

            String json = JSON.toJSONString(userSession);
            Map<String, Object> map = JSONObject.parseObject(json);

            this.securityRedisUtil.set(tokenKey, map);
            this.securityRedisUtil.expire(tokenKey, 2592000L);
            this.securityRedisUtil.sSet(userSessionKey, new Object[]{tokenKey});
            return true;
        } catch (Exception var7) {
            log.error("延长token过期时间", var7);
            return false;
        }
    }

    public String getClientIp(ServerHttpRequest serverHttpRequest) {

        String ip = parseHeader(serverHttpRequest, "x-forwarded-for");
        if (!StrUtil.isEmptyOrUndefined(ip)) {
            ip = parseHeader(serverHttpRequest, "Proxy-Client-IP");
        }
        if (!StrUtil.isEmptyOrUndefined(ip)) {
            ip = parseHeader(serverHttpRequest, "WL-Proxy-Client-IP");
        }
        if (!StrUtil.isEmptyOrUndefined(ip)) {
            ip = Objects.requireNonNull(serverHttpRequest.getRemoteAddress()).toString();
        }

        if (StrUtil.isNotBlank(ip)) {
            // 多个路由时，取第一个非unknown的ip
            final String[] arr = ip.split(",");
            for (final String str : arr) {
                if (!"unknown".equalsIgnoreCase(str)) {
                    ip = str;
                    break;
                }
            }
        } else {
            ip = "--";
        }

        return ip;
    }

}
