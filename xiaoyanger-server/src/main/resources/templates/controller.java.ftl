package ${package.Controller};

import ${package.Entity}.${entity};
import ${package.Service}.${table.serviceName};
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import BeanCommonUtils;
import QueryVO;
import ResultDataVO;
import ResultListVO;
import ResultVO;

import ${package.Other}.${entity}VO;
/**
 * ${table.comment!} 前端控制器
 *
 * @author ${author}
 * @date ${date}
 */
@Api(tags = "${table.comment!}接口")
@RestController
@RequestMapping("/${table.entityPath}")
public class ${table.controllerName}{

    @Autowired
    private ${table.serviceName} ${table.serviceName?uncap_first};

    @ApiOperation("分页查询")
    @PostMapping("/page")
    public ResultListVO<${entity}VO> getPage(@RequestBody QueryVO queryVO){
        IPage<${entity}VO> ${entity}VOList = ${table.serviceName?uncap_first}.queryPage(queryVO);
        return ResultListVO.ok(${entity}VOList);
    }

    @ApiOperation("新增")
    @PostMapping("/create")
    public ResultVO<String> save(@RequestBody ${entity}VO ${table.entityPath}VO){
        ${entity} ${table.entityPath} = new ${entity}();
        BeanCommonUtils.copyProperties(${table.entityPath}VO, ${table.entityPath});
        ${table.serviceName?uncap_first}.save(${table.entityPath});
        return ResultVO.ok();
    }

    @ApiOperation("删除")
    @DeleteMapping("/delete")
    public ResultVO<String> delete(@ApiParam("数据对象id") @RequestParam("id")${table.fields[0].propertyType} ${table.fields[0].propertyName}){
        ${table.serviceName?uncap_first}.removeById(id, true);
        return ResultVO.ok();
    }

    @ApiOperation("更新")
    @PutMapping("/update")
    public ResultVO<String> update(@RequestBody ${entity}VO ${table.entityPath}VO){
        ${entity} ${table.entityPath} = new ${entity}();
        BeanCommonUtils.copyProperties(${table.entityPath}VO, ${table.entityPath});
        ${table.serviceName?uncap_first}.updateById(${table.entityPath});
        return ResultVO.ok();
    }

    @ApiOperation("通过ID获取一条数据")
    @GetMapping("/read")
    public ResultDataVO<${entity}VO> getById(@ApiParam("数据对象id") @RequestParam("id")${table.fields[0].propertyType} ${table.fields[0].propertyName}){
        ${entity} ${table.entityPath} = ${table.serviceName?uncap_first}.getById(id);
        ${entity}VO ${table.entityPath}VO = null;
        if(${table.entityPath} != null){
            ${table.entityPath}VO = new ${entity}VO();
            BeanCommonUtils.copyProperties(${table.entityPath}, ${table.entityPath}VO);
        }
        return ResultDataVO.ok(${table.entityPath}VO);
    }

}
