package ${package.ServiceImpl};

import ${package.Entity}.${entity};
import ${package.Mapper}.${table.mapperName};
import ${package.Service}.${table.serviceName};
import ${superServiceImplClassPackage};
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import BeanCommonUtils;
import Query;
import QueryVO;
import com.google.common.collect.Lists;
import ${package.Other}.${entity}VO;
import java.util.*;
/**
 * ${table.comment!} 服务实现类
 *
 * @author ${author}
 * @date ${date}
 */
@Service
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${table.serviceImplName} extends ${superServiceImplClass}<${table.mapperName}, ${entity}> implements ${table.serviceName} {

     @Override
     public IPage<${entity}VO> queryPage(QueryVO queryVO){
          LambdaQueryWrapper<${entity}> queryWrapper = new LambdaQueryWrapper<>();
          Map<String, Object> query = queryVO.getQuery();
          IPage<${entity}> pageList = this.page(new Query<${entity}>().getPage(queryVO), queryWrapper);
          List<${entity}> records = pageList.getRecords();
          List<${entity}VO> ${entity?uncap_first}VOList = Lists.newArrayList();
          BeanCommonUtils.copyListProperties(records, ${entity?uncap_first}VOList, ${entity}VO.class);
          Page<${entity}VO> page = new Page<>(pageList.getCurrent(), pageList.getSize(), pageList.getTotal());
          page.setRecords(${entity?uncap_first}VOList);
          return page;
     }

}
</#if>
