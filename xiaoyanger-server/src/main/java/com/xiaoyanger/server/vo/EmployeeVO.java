package com.xiaoyanger.server.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 员工表 VO对象
 *
 * @author ningjiangbo
 * @date 2023-04-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "EmployeeVO对象", description = "员工表")
public class EmployeeVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 标识
     */
    @ApiModelProperty(value = "标识")
    private String id;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人")
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;

    /**
     * 已删除
     */
    @ApiModelProperty(value = "已删除")
    private Boolean deleted;

    /**
     * 1-正常,2-冻结
     */
    @ApiModelProperty(value = "1-正常,2-冻结")
    private String status;

    /**
     * 员工姓名
     */
    @ApiModelProperty(value = "员工姓名")
    private String employeeName;

    /**
     * 生日
     */
    @ApiModelProperty(value = "生日")
    private LocalDateTime birthday;

    /**
     * 电话号码
     */
    @ApiModelProperty(value = "电话号码")
    private String phoneNo;

    /**
     * 工号
     */
    @ApiModelProperty(value = "工号")
    private String workNo;

    /**
     * 性别(0-默认未知,1-男,2-女)
     */
    @ApiModelProperty(value = "性别(0-默认未知,1-男,2-女)")
    private String sex;

    /**
     * 电子邮件
     */
    @ApiModelProperty(value = "电子邮件")
    private String email;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;

    /**
     * md5密码盐
     */
    @ApiModelProperty(value = "md5密码盐")
    private String salt;


}
