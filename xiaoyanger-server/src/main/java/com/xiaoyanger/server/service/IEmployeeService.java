package com.xiaoyanger.server.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaoyanger.core.utils.vo.QueryVO;
import com.xiaoyanger.server.entity.Employee;
import com.xiaoyanger.server.vo.EmployeeVO;

/**
 * 员工表 服务类
 *
 * @author ningjiangbo
 * @date 2023-04-13
 */
public interface IEmployeeService extends IService<Employee> {

    /**
     * 分页查询
     */
    IPage<EmployeeVO> queryPage(QueryVO queryVO);

}
