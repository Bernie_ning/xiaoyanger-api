package com.xiaoyanger.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.xiaoyanger.core.utils.BeanCommonUtils;
import com.xiaoyanger.core.utils.Query;
import com.xiaoyanger.core.utils.vo.QueryVO;
import com.xiaoyanger.server.dao.EmployeeMapper;
import com.xiaoyanger.server.entity.Employee;
import com.xiaoyanger.server.service.IEmployeeService;
import com.xiaoyanger.server.vo.EmployeeVO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 员工表 服务实现类
 *
 * @author ningjiangbo
 * @date 2023-04-13
 */
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements IEmployeeService {

     @Override
     public IPage<EmployeeVO> queryPage(QueryVO queryVO){
          LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
          Map<String, Object> query = queryVO.getQuery();
          IPage<Employee> pageList = this.page(new Query<Employee>().getPage(queryVO), queryWrapper);
          List<Employee> records = pageList.getRecords();
          List<EmployeeVO> employeeVOList = Lists.newArrayList();
          BeanCommonUtils.copyListProperties(records, employeeVOList, EmployeeVO.class);
          Page<EmployeeVO> page = new Page<>(pageList.getCurrent(), pageList.getSize(), pageList.getTotal());
          page.setRecords(employeeVOList);
          return page;
     }

}
