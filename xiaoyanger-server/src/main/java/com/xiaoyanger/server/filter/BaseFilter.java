package com.xiaoyanger.server.filter;

import com.xiaoyanger.core.exception.BusinessException;
import com.xiaoyanger.core.exception.SystemErrorType;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * 请求拦截，避免服务绕过接口被直接访问
 * @author ningjiangbo
 * @since 2023/04/25 10:13
 */
@Component
@WebFilter
public class BaseFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String gateway = request.getHeader("X-Gateway");
        if (StringUtils.isEmpty(gateway)) {
            throw new BusinessException(SystemErrorType.SYSTEM_GATEWAY);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }

}