package com.xiaoyanger.server.controller.manager;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xiaoyanger.core.context.CommonUserInfoDTO;
import com.xiaoyanger.core.context.CurrentUserInfo;
import com.xiaoyanger.core.utils.BeanCommonUtils;
import com.xiaoyanger.core.utils.vo.QueryVO;
import com.xiaoyanger.core.utils.vo.ResultDataVO;
import com.xiaoyanger.core.utils.vo.ResultListVO;
import com.xiaoyanger.core.utils.vo.ResultVO;
import com.xiaoyanger.server.entity.Employee;
import com.xiaoyanger.server.service.IEmployeeService;
import com.xiaoyanger.server.vo.EmployeeVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

/**
 * 员工表 前端控制器
 *
 * @author ningjiangbo
 * @date 2023-04-13
 */
@Api(tags = "员工表接口")
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private IEmployeeService iEmployeeService;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @ApiOperation("分页查询")
    @PostMapping("/page")
    public ResultListVO<EmployeeVO> getPage(@RequestBody QueryVO queryVO) {
        IPage<EmployeeVO> EmployeeVOList = iEmployeeService.queryPage(queryVO);
        return ResultListVO.ok(EmployeeVOList);
    }

    @GetMapping("/redisSet")
    public Object redisSet(@RequestParam("id") String id) {
        System.out.println(id);

        CommonUserInfoDTO userInfoDTO = CurrentUserInfo.getCurrUserInfo();
        System.out.println(userInfoDTO.getId());
        Object o = redisTemplate.opsForValue().get("Access-Token:PC:" + id);
        System.out.println(o);
        return o;

    }

    @ApiOperation("新增")
    @PostMapping("/create")
    public ResultVO<String> save(@RequestBody EmployeeVO employeeVO) {
        Employee employee = new Employee();
        BeanCommonUtils.copyProperties(employeeVO, employee);
        iEmployeeService.save(employee);
        return ResultVO.ok();
    }

    @ApiOperation("删除")
    @DeleteMapping("/delete")
    public ResultVO<String> delete(@ApiParam("数据对象id") @RequestParam("id") String id) {
        iEmployeeService.removeById(id, true);
        return ResultVO.ok();
    }

    @ApiOperation("更新")
    @PutMapping("/update")
    public ResultVO<String> update(@RequestBody EmployeeVO employeeVO) {
        Employee employee = new Employee();
        BeanCommonUtils.copyProperties(employeeVO, employee);
        iEmployeeService.updateById(employee);
        return ResultVO.ok();
    }

    @ApiOperation("通过ID获取一条数据")
    @GetMapping("/read")
    public ResultDataVO<EmployeeVO> getById(@ApiParam("数据对象id") @RequestParam("id") String id) {
        Employee employee = iEmployeeService.getById(id);
        EmployeeVO employeeVO = null;
        if (employee != null) {
            employeeVO = new EmployeeVO();
            BeanCommonUtils.copyProperties(employee, employeeVO);
        }
        return ResultDataVO.ok(employeeVO);
    }

}
