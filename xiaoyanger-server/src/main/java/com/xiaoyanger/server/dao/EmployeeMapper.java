package com.xiaoyanger.server.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaoyanger.server.entity.Employee;
import org.apache.ibatis.annotations.Mapper;

/**
 * 员工表 Dao 接口
 *
 * @author ningjiangbo
 * @date 2023-04-13
 */
@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {

}
