package com.xiaoyanger.server.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 员工表实体对象
 *
 * @author ningjiangbo
 * @date 2023-04-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("employee")
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 标识
     */
    private String id;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 已删除
     */
    private Boolean deleted;

    /**
     * 1-正常,2-冻结
     */
    private String status;

    /**
     * 员工姓名
     */
    private String employeeName;

    /**
     * 生日
     */
    private LocalDateTime birthday;

    /**
     * 电话号码
     */
    private String phoneNo;

    /**
     * 工号
     */
    private String workNo;

    /**
     * 性别(0-默认未知,1-男,2-女)
     */
    private String sex;

    /**
     * 电子邮件
     */
    private String email;

    /**
     * 密码
     */
    private String password;

    /**
     * md5密码盐
     */
    private String salt;


}
