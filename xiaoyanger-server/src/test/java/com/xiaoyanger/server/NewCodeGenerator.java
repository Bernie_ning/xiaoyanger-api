package com.xiaoyanger.server;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.IConfigBuilder;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.DateType;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @author ningjiangbo
 * @desc mybatis-plus逆向工程，3.5.1版本
 * 执行 main 方法控制台输入模块表名回车自动生成对应项目目录中
 * @create 2021/12/23
 **/
public class NewCodeGenerator {

    /**
     * 读取控制台输入内容
     */
    private static final Scanner SCANNER = new Scanner(System.in);

    /**
     * 控制台输入内容读取并打印提示信息
     *
     * @param message 提示信息
     * @return
     */
    public static String scannerNext(String message) {
        System.out.println(message);
        String nextLine = SCANNER.nextLine();
        if (StringUtils.isBlank(nextLine)) {
            // 如果输入空行继续等待
            return SCANNER.next();
        }
        return nextLine;
    }

    protected static <T> T configBuilder(IConfigBuilder<T> configBuilder) {
        return null == configBuilder ? null : configBuilder.build();
    }

    public static void main(String[] args) {
        genExecute();
    }

    private static void genExecute() {
        final String MYSQL_HOST = System.getProperty("MYSQL-HOST") != null ? System.getProperty("MYSQL-HOST") : "mysql-tcp-test.cloudfame.com";
        final String MYSQL_PORT_YF = System.getProperty("MYSQL-PORT-YF") != null ? System.getProperty("MYSQL-PORT-YF") : "3306";
        final String MYSQL_USER = System.getProperty("MYSQL-USER") != null ? System.getProperty("MYSQL-USER") : "cloudfame_dev1";
        final String MYSQL_PWD = System.getProperty("MYSQL-PWD") != null ? System.getProperty("MYSQL-PWD") : "IVYC3uk6friB0f1x";
        final String MYSQL_DB = System.getProperty("MYSQL-DB") != null ? System.getProperty("MYSQL-DB") : "cloudfame_medata_dev1";

        final String DB_URL = "jdbc:mysql://182.92.242.246:3306/hairdressing?charset=utf8mb4&prepStmtCacheSize=517&cachePrepStmts=true&autoReconnect=true&characterEncoding=utf-8&allowMultiQueries=true&useSSL=false&serverTimezone=Asia/Shanghai";
        final String DB_USER_NAME = "root";
        final String DB_PASSWORD = "1@3$qWeR";

        // final String PACKAGE_NAME = "modules." + scannerNext("请输入包名：");
        final String parent = "com.xiaoyanger";

        // 代码生成器
        String projectPath = System.getProperty("user.dir") + "\\" + "xiaoyanger-server";

        FastAutoGenerator.create(DB_URL, DB_USER_NAME, DB_PASSWORD)
                .globalConfig(builder -> {
                    builder.author("ningjiangbo") // 设置作者
                            .enableSwagger() // 开启 swagger 模式
                            .disableOpenDir().dateType(DateType.TIME_PACK)
                            .fileOverride() // 覆盖已生成文件
                            .outputDir(StringUtils.isNotBlank(projectPath) ? projectPath + "/src/main/java" : "D://逆向工程")  // 指定输出目录
                            .build();
                })
                .packageConfig(builder -> {
                    Map<OutputFile, String> pathInfo = new HashMap<>();
                    String xmlPath = projectPath + "/src/main/resources/mapper/"
                            // + PACKAGE_NAME.replace(".", "/") + "/"
                            ;
                    //String voPath = projectPath + "/src/main/java/" + parent.replace(".","/")+"/"+ PACKAGE_NAME.replace(".","/")+ "/";
                    pathInfo.put(OutputFile.mapperXml, xmlPath);
                    //pathInfo.put(OutputFile.other, voPath);
                    builder.parent(parent) // 设置父包名
                            // .moduleName(PACKAGE_NAME) // 设置父包模块名
                            .controller("controller.manager")
                            .mapper("dao")
                            .other("vo")
                                .pathInfo(pathInfo).build(); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.addInclude(scannerNext("请输入表名（英文逗号分隔）：").split(",")) // 设置需要生成的表名
                            .addTablePrefix("t_", "c_") // 设置过滤表前缀
                            .build().entityBuilder()
                            // .versionColumnName(Constant.REVISION) // 基于数据库字段
                            // .logicDeleteColumnName(Constant.DELETED) // 基于数据库字段
                            // .addTableFills(new Property(Constant.CREATE_TIME, FieldFill.INSERT))    //基于数据库字段填充
                            // .addTableFills(new Property(Constant.CREATE_ID, FieldFill.INSERT))    //基于数据库字段填充
                            // .addTableFills(new Property(Constant.UPDATE_ID, FieldFill.INSERT_UPDATE))    //基于模型属性填充
                            // .addTableFills(new Property(Constant.UPDATE_TIME, FieldFill.INSERT_UPDATE))
                            .enableLombok()
                            .build();

                })
                .injectionConfig(builder -> {
                    Map<String, String> customFile = new HashMap<>();
                    customFile.put("domain/vo:VO", "/templates/entityVO.java");
                    builder.beforeOutputFile((tableInfo, objectMap) -> {
// 自                    //定义 Mapper XML 生成目录
//                        ConfigBuilder config = (ConfigBuilder) objectMap.get("config");
//                        Map<OutputFile, String> pathInfoMap = config.getPathInfo();
//                        pathInfoMap.put(OutputFile.mapperXml, pathInfoMap.get(OutputFile.mapperXml).replaceAll("/java.*", "/resources/mapper"));
//                        objectMap.put("config", config);

                    }).customFile(customFile).build();

                })
                .templateEngine(new MyFreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }
}
